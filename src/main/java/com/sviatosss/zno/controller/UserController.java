package com.sviatosss.zno.controller;

import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.payload.request.UserUpdateRequest;
import com.sviatosss.zno.service.UserService;
import com.sviatosss.zno.utils.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/current")
    public User getCurrentUser() {
        User user = userService.getCurrentUserFromDB();
//        logger.info(String.valueOf(user));
        return user;
    }

    @GetMapping("/search/{value}")
    public ResponseEntity<?> findByUsernameOrPhone(@PathVariable String value) {
        User user = userService.findByUsernameOrPhone(value);
        if (user != null){
            return new ResponseEntity<>(user, HttpStatus.OK);
        }else return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/recovery-email/{email}")
    public ResponseEntity<?> updatePasswordByEmail(@PathVariable String email) {
        int idUser = userService.updatePasswordByEmail(email);
        if (idUser != 0){
            return new ResponseEntity<>(idUser, HttpStatus.OK);
        }else return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
    }

    @PutMapping("")
    public ResponseEntity<?> updateUser(@RequestBody UserUpdateRequest user,
                                        BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new Validator().getErrors(bindingResult);
        }else {
            return new ResponseEntity<>(userService.updateUser(user), HttpStatus.OK);
        }
    }
}