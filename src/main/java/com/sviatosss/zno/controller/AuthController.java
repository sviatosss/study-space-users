package com.sviatosss.zno.controller;

import com.sviatosss.zno.event.OnRegistrationCompleteEvent;
import com.sviatosss.zno.payload.request.LoginRequest;
import com.sviatosss.zno.payload.request.SignUpRequest;
import com.sviatosss.zno.payload.request.SignUpWebRequest;
import com.sviatosss.zno.payload.response.JwtResponse;
import com.sviatosss.zno.payload.response.ValidResponse;
import com.sviatosss.zno.service.UserAuthService;
import com.sviatosss.zno.service.VerificationPhoneService;
import com.sviatosss.zno.service.additional.sms.SmsService;
import com.sviatosss.zno.utils.*;
import com.sviatosss.zno.utils.validator.EmailValidator;
import com.sviatosss.zno.utils.validator.OptValidator;
import com.sviatosss.zno.utils.validator.PhoneValidator;
import com.sviatosss.zno.utils.validator.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserAuthService userAuthService;

    @Autowired
    public OtpService otpService;

    @Autowired
    public SmsService smsService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private VerificationPhoneService verificationPhoneService;

    @PostMapping("/sign-in")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, BindingResult bindingResult) {
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }
        logger.info(String.valueOf(loginRequest));
        return userAuthService.signIn(loginRequest);
    }

    @Transactional
    @PostMapping("/sign-up/{deviceId}")
    public ResponseEntity<?> registerUser(HttpServletRequest request, @Valid @RequestBody SignUpRequest signUpRequest, BindingResult bindingResult, @PathVariable String deviceId) {
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }
        Long idValidationPhone = verificationPhoneService.delete(signUpRequest.getPhone(), deviceId);
        if (idValidationPhone == 0){
            Map<String, String> hm = new HashMap<>();
            hm.put("phone", "телефон бути веретифікованим !");
            return new ResponseEntity<>(hm, HttpStatus.BAD_REQUEST);
        }

        JwtResponse jwtResponse = userAuthService.signUp(signUpRequest);

        boolean isEmail = false;
        if (signUpRequest.getEmail() != null) isEmail = true;

        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(isEmail, signUpRequest.getReferral(), request.getRemoteAddr(), jwtResponse));

        System.out.println("return");
        return ResponseEntity.ok(jwtResponse);
    }

    @Transactional
    @PostMapping("/sign-up")
    public ResponseEntity<?> checkOptAndRegisterUser(HttpServletRequest request, @Valid @RequestBody SignUpWebRequest signUpRequest, BindingResult bindingResult) {
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }

        if(signUpRequest.getOtpnum() >= 0){
            int serverOtp = otpService.getOtp(signUpRequest.getPhone());
            if(serverOtp > 0) {
                if (signUpRequest.getOtpnum() == serverOtp) {
                    otpService.clearOTP(signUpRequest.getPhone());
                }else {
                    Map<String, String> hm = new HashMap<>();
                    hm.put("otpnum", "код підтвердження неправильний !");
                    return new ResponseEntity<>(hm, HttpStatus.BAD_REQUEST);
                }
            }else {
                Map<String, String> hm = new HashMap<>();
                hm.put("otpnum", "код підтвердження неправильний !");
                return new ResponseEntity<>(hm, HttpStatus.BAD_REQUEST);
            }
        }


        JwtResponse jwtResponse = userAuthService.signUp(new SignUpRequest(signUpRequest));

        boolean isEmail = false;
        if (signUpRequest.getEmail() != null) isEmail = true;

        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(isEmail, signUpRequest.getReferral(), request.getRemoteAddr(), jwtResponse));

        System.out.println("return");
        return ResponseEntity.ok(jwtResponse);
    }

    @GetMapping("/check/username/{username}")
    public ResponseEntity<ValidResponse> checkUsername(@PathVariable String username) {
        return new ResponseEntity<>(new ValidResponse(userAuthService.validUsername(username)), HttpStatus.OK);
    }

    @GetMapping("/check/phone/{phone}")
    public ResponseEntity<?> checkPhone(@PathVariable String phone) {
        ResponseEntity<?> responseEntity = PhoneValidator.validate(phone);
        if (responseEntity != null){
            return responseEntity;
        }
        return new ResponseEntity<>(new ValidResponse(userAuthService.validPhone(phone)), HttpStatus.OK);
    }

    @GetMapping("/check/email/{email}")
    public ResponseEntity<?> checkEmail(@PathVariable String email) {
        ResponseEntity<?> responseEntity = EmailValidator.validate(email);
        if (responseEntity != null){
            return responseEntity;
        }
        boolean valid = userAuthService.validEmail(email);
        return new ResponseEntity<>(new ValidResponse(valid), valid ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/send-sms/{phone}")
    public ResponseEntity<?> generateOtp(@PathVariable String phone){
        ResponseEntity<?> responseEntity = PhoneValidator.validate(phone);
        if (responseEntity != null){
            return responseEntity;
        }

        int otp = otpService.generateOTP(phone);

//        logger.info("OTP : " + otp);

        smsService.send(phone, String.valueOf(otp));

        Map<String, Integer> code = new HashMap<>();
        code.put("code", otp);

        return new ResponseEntity<>(code, HttpStatus.OK);
    }

    @GetMapping("/validation/{deviceId}/{phone}/{otpnum}")
    public ResponseEntity<?> validateOtp(@PathVariable String deviceId, @PathVariable String phone, @PathVariable int otpnum){
//        logger.info("Otp Number : " + otpnum);
        ResponseEntity<?> responseEntity = PhoneValidator.validate(phone);
        if (responseEntity != null){
            return responseEntity;
        }

        ResponseEntity<?> responseEntity2 = OptValidator.validate(String.valueOf(otpnum));
        if (responseEntity2 != null){
            return responseEntity2;
        }

        if(otpnum >= 0){
            int serverOtp = otpService.getOtp(phone);
            if(serverOtp > 0) {
                if (otpnum == serverOtp) {
                    otpService.clearOTP(phone);
                    verificationPhoneService.save(phone, deviceId);
                    return new ResponseEntity<>(new  ValidResponse(true), HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>(new  ValidResponse(false), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_STUDENT', 'ROLE_TEACHER', 'ROLE_CONTENT_MANAGER', 'ROLE_ADMIN')")
    @PostMapping("/update-jwt")
    public ResponseEntity<?> updateJwt() {
        Map<String, String> token = new HashMap<>();
        token.put("token", userAuthService.updateJWT());
        return new ResponseEntity<>(token, HttpStatus.OK);
    }

}