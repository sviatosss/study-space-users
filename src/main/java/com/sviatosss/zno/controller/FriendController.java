package com.sviatosss.zno.controller;

import com.sviatosss.zno.entity.Enums.ERelationshipStatus;
import com.sviatosss.zno.entity.Enums.ЕTypeNotification;
import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.entity.UserNotification;
import com.sviatosss.zno.event.OnUserNotificationEvent;
import com.sviatosss.zno.payload.response.FriendResponse;
import com.sviatosss.zno.service.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/friend")
public class FriendController {
    @Autowired
    private FriendService friendService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @GetMapping("/all")
    public ResponseEntity<?> getFriends() {
        List<User> users = friendService.getFriends();
        if (users.size() != 0){
            return new ResponseEntity<>(FriendResponse.createFromList(users), HttpStatus.OK);
        }
        return new ResponseEntity<>("no friends", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/add/{friend_id}")
    public ResponseEntity<String> addFriend(@Valid @PathVariable Long friend_id) {
        Long currentUser = friendService.setFriendshipStatus(friend_id, ERelationshipStatus.PENDING);
        if (currentUser != null){
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            eventPublisher.publishEvent(new OnUserNotificationEvent(new UserNotification(
                    (long) 1, currentUser, userDetails.getUsername(), friend_id, ЕTypeNotification.FRIEND_REQUEST)));
            System.out.println("return");
            return new ResponseEntity<>("added", HttpStatus.OK);
        }
        return new ResponseEntity<>("error", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/accept/{friend_id}")
    public ResponseEntity<?> friendAccept(@Valid @PathVariable Long friend_id) {
        Long add = friendService.setFriendshipStatus(friend_id, ERelationshipStatus.ACCEPTED);
        if (add != null){
            return new ResponseEntity<>("friend accepted", HttpStatus.OK);
        }else return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/deny/{friend_id}")
    public ResponseEntity<?> friendDenied(@Valid @PathVariable Long friend_id) {
        Long denied = friendService.setFriendshipStatus(friend_id, ERelationshipStatus.DECLINED);
        if (denied != null){
            return new ResponseEntity<>("friend denied", HttpStatus.OK);
        }else return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/block/{friend_id}")
    public ResponseEntity<?> friendBlock(@Valid @PathVariable Long friend_id) {
        Long denied = friendService.setFriendshipStatus(friend_id, ERelationshipStatus.BLOCKED);
        if (denied != null){
            return new ResponseEntity<>("friend blocked", HttpStatus.OK);
        }else return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
    }
}