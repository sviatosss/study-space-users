package com.sviatosss.zno.controller;

import com.sviatosss.zno.entity.Enums.ESubject;
import com.sviatosss.zno.entity.Statistic;
import com.sviatosss.zno.payload.response.ValidResponse;
import com.sviatosss.zno.service.StatisticService;
import com.sviatosss.zno.utils.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/statistic")
public class StatisticController {

//    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private StatisticService service;

    @PostMapping("")
    public ResponseEntity<?> addStatistic(@Valid @RequestBody Statistic statistic, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new Validator().getErrors(bindingResult);
        }else {
            return new ResponseEntity<>(service.create(statistic), HttpStatus.OK);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<?> StatisticFindById(@PathVariable Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<?> FindUserStatistic() {
        return new ResponseEntity<>(service.findUserStatistic(), HttpStatus.OK);
    }

    @GetMapping("/{subject}")
    public ResponseEntity<?> FindUserStatistic(@PathVariable ESubject subject) {
        return new ResponseEntity<>(service.findUserStatisticBySubject(subject), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> StatisticDeleteById(@PathVariable Long id) {
        service.findById(id);
        service.deleteById(id);
        return new ResponseEntity<>(new ValidResponse(true), HttpStatus.OK);
    }
}