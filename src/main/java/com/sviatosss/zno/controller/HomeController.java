package com.sviatosss.zno.controller;

import com.sviatosss.zno.entity.Referral;
import com.sviatosss.zno.payload.response.ValidResponse;
import com.sviatosss.zno.service.ReferralService;
import com.sviatosss.zno.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
public class HomeController {
    @Autowired
    private ReferralService referralService;

    @Autowired
    private UserService userService;

    @GetMapping("")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "index";
    }

    @GetMapping("/user/{id}/email-verification/{token}")
    @ResponseBody
    public ResponseEntity<ValidResponse> emailVerification(@PathVariable Long id, @PathVariable String token) {
        return ResponseEntity.ok(new ValidResponse(userService.verificationToken(id, token)));
    }

    @GetMapping(value = "/referral/{username}")
    public void method(@PathVariable String username, HttpServletResponse httpServletResponse, HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        String userAgent = request.getHeader("User-Agent");

        referralService.save(new Referral(ip, userAgent, username));

        if (userAgent.contains("Android")){
            httpServletResponse.setHeader("Location", "https://play.google.com/store");
            httpServletResponse.setStatus(302);
        }else if (userAgent.contains("Ios")){
            httpServletResponse.setHeader("Location", "https://www.apple.com/ru/ios/app-store");
            httpServletResponse.setStatus(302);
        }else {
            httpServletResponse.setHeader("Location", "https://www.google.com");
            httpServletResponse.setStatus(302);
        }
    }
}



