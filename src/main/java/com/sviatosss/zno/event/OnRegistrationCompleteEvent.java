package com.sviatosss.zno.event;

import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.payload.response.JwtResponse;
import org.springframework.context.ApplicationEvent;

@SuppressWarnings("serial")
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private boolean isEmail;
    private String referral;
    private String ip;
    private final User user;

    public OnRegistrationCompleteEvent(boolean isEmail, String referral, String ip, final JwtResponse user) {
        super(user);
        this.isEmail = isEmail;
        this.referral = referral;
        this.ip = ip;
        if (isEmail){
            this.user = new User(user.getId(), user.getUsername(), user.getPhone(), user.getEmail());
        }else this.user = new User(user.getId(), user.getUsername(), user.getPhone());
    }

    public User getUser() {
        return user;
    }

    public boolean isEmail() {
        return isEmail;
    }

    public String getReferral() {
        return referral;
    }

    public String getIp() {
        return ip;
    }
}
