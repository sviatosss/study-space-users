package com.sviatosss.zno.event.listeners;

import com.sviatosss.zno.entity.UserNotification;
import com.sviatosss.zno.event.OnUserNotificationEvent;
import com.sviatosss.zno.service.additional.notification.UserNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class UserNotificationListener implements ApplicationListener<OnUserNotificationEvent> {

    @Autowired
    UserNotificationService service;

    @Override
    public void onApplicationEvent(final OnUserNotificationEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final OnUserNotificationEvent event) {
        final UserNotification userNotification = event.getUserNotification();
        if (userNotification.getType() != null) {
            switch (userNotification.getType()) {
                case FRIEND_REQUEST: service.sendFriendRequestNotification(userNotification); break;
                case DUEL_REQUEST: System.out.println("DUEL REQUEST"); break;
                case FRIEND_NOTE: System.out.println("FRIEND NOTE"); break;
                default: System.out.println("null");
            }
        }else System.out.println("null");
    }
}
