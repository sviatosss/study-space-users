package com.sviatosss.zno.event.listeners;

import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.event.OnRegistrationCompleteEvent;
import com.sviatosss.zno.service.ReferralService;
import com.sviatosss.zno.service.UserService;
import com.sviatosss.zno.service.additional.email.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    @Autowired
    private Environment env;

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @Autowired
    private ReferralService referralService;

    // API

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final OnRegistrationCompleteEvent event) {
        final User user = event.getUser();

        if (event.isEmail()) {
            final String token = UUID.randomUUID().toString();
            userService.createVerificationToken(user, token);
            mailService.sendingEmailVerification(user.getEmail(), user.getId(), token);
            System.out.println("sending token");
        }

        if (event.getReferral() != null){
            userService.addReferral(event.getReferral());
            System.out.println("add referral");
        }else {
            referralService.checkReferral(event.getIp());
            System.out.println("check if exit referral");
        }



//        final SimpleMailMessage email = constructEmailMessage(event, user, token);
//        mailSender.send(email);
    }

    //

//    private SimpleMailMessage constructEmailMessage(final OnRegistrationCompleteEvent event, final User user, final String token) {
//        final String recipientAddress = user.getEmail();
//        final String subject = "Registration Confirmation";
//        final String confirmationUrl = event.getAppUrl() + "/registrationConfirm.html?token=" + token;
//        final String message = messages.getMessage("message.regSuccLink", null, "You registered successfully. To confirm your registration, please click on the below link.", event.getLocale());
//        final SimpleMailMessage email = new SimpleMailMessage();
//        email.setTo(recipientAddress);
//        email.setSubject(subject);
//        email.setText(message + " \r\n" + confirmationUrl);
//        email.setFrom(env.getProperty("support.email"));
//        return email;
//    }
}
