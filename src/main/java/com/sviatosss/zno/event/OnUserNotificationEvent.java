package com.sviatosss.zno.event;

import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.entity.UserNotification;
import com.sviatosss.zno.payload.response.JwtResponse;
import org.springframework.context.ApplicationEvent;

@SuppressWarnings("serial")
public class OnUserNotificationEvent extends ApplicationEvent {
    private final UserNotification userNotification;

    public OnUserNotificationEvent(UserNotification userNotification) {
        super(userNotification);
        this.userNotification = userNotification;
    }

    public UserNotification getUserNotification() {
        return userNotification;
    }
}
