package com.sviatosss.zno.utils.validator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PhoneValidator {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^\\d{9}$", Pattern.CASE_INSENSITIVE);

    public static boolean isValid(String phone) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(phone);
        return matcher.find();
    }

    public static ResponseEntity<?> validate(String phone) {
        if (!isValid(phone)){

            Map<String, String> hm = new HashMap<>();
            hm.put("phone", "телефон повинен містити 9 цифер");

            return new ResponseEntity<>(hm, HttpStatus.BAD_REQUEST);
        }else return null;
    }
}