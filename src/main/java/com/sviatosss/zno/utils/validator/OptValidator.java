package com.sviatosss.zno.utils.validator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class OptValidator {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^\\d{6}$", Pattern.CASE_INSENSITIVE);

    public static boolean isValid(String opt) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(opt);
        return matcher.find();
    }

    public static ResponseEntity<?> validate(String opt) {
        if (!isValid(opt)){

            Map<String, String> hm = new HashMap<>();
            hm.put("opt", "opt повинен містити 6 цифер");

            return new ResponseEntity<>(hm, HttpStatus.BAD_REQUEST);
        }else return null;
    }
}