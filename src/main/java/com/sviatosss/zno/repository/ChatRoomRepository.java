package com.sviatosss.zno.repository;

import com.sviatosss.zno.entity.ChatRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatRoomRepository extends JpaRepository<ChatRoom, Long> {
    Optional<ChatRoom> findByChatIdAndSenderIdAndRecipientId(String chatId, Long senderId, Long recipientId);
}