package com.sviatosss.zno.repository;

import com.sviatosss.zno.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByPhone(String phone);

    Optional<User> findAllByUsernameOrPhone(String username, String phone);

    List<User> findAllByIdIn(List<Long> userIds);

    boolean existsById(Long id);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Boolean existsByPhone(String phone);


    @Modifying
    @Query("update User u set u.active = TRUE where u.id = :id")
    void updateActive(@Param(value = "id") long id);

    @Modifying
    @Query("update User u set u.password = :password where (u.email = :email AND u.active = TRUE)")
    int updatePasswordByEmail(@Param(value = "password") String password, @Param(value = "email") String email);

    @Modifying
    @Query("update User u set u.password = :password where u.phone = :phone")
    int updatePasswordByPhone(@Param(value = "password") String password, @Param(value = "phone") String phone);
}