package com.sviatosss.zno.repository;

import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.entity.VerificationPhone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.stream.Stream;

public interface VerificationPhoneRepository extends JpaRepository<VerificationPhone, Long> {
    Long deleteByPhoneAndDeviceId(String phone, String deviceId);
}