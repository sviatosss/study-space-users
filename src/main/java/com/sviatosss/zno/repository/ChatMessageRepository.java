package com.sviatosss.zno.repository;

import com.sviatosss.zno.entity.ChatMessage;
import com.sviatosss.zno.entity.Enums.MessageStatus;
import com.sviatosss.zno.entity.Referral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {
    List<ChatMessage> findByChatId(String chatId);

    long countBySenderIdAndRecipientIdAndStatus(
            Long senderId, Long recipientId, MessageStatus status);


    @Modifying
    @Query("update ChatMessage u set u.status = :status where (u.senderId = :senderId AND u.recipientId = :recipientId)")
    void updateStatus( @Param(value = "senderId") Long senderId, @Param(value = "recipientId") Long recipientId, @Param(value = "status") MessageStatus status);
}