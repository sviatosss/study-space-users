package com.sviatosss.zno.repository;

import com.sviatosss.zno.entity.Enums.ESubject;
import com.sviatosss.zno.entity.Statistic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StatisticRepository extends JpaRepository<Statistic, Long> {
    Optional<Statistic> findById(Long id);
    List<Statistic> findAllByUserId(Long userId);
    List<Statistic> findAllByUserIdAndSubject(Long userId, ESubject subject);
}