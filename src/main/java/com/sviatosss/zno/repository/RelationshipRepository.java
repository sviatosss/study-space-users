package com.sviatosss.zno.repository;

import com.sviatosss.zno.entity.Enums.ERelationshipStatus;
import com.sviatosss.zno.entity.Relationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RelationshipRepository extends JpaRepository<Relationship, Long> {

    @Query("SELECT u FROM Relationship u WHERE (user_one_id = ?1 OR user_two_id = ?1) AND status = ?2")
    List<Relationship> findFriends(Long id, ERelationshipStatus status);

    Boolean existsByUserOneIdAndUserTwoId(Long userOneId, Long userTwoId);

    Boolean existsByUserOneIdAndUserTwoIdAndStatus(
            Long userOneId, Long userTwoId, ERelationshipStatus status);

    Boolean existsByUserOneIdAndUserTwoIdAndStatusAndActionUserId(
            Long userOneId, Long userTwoId, ERelationshipStatus status, Long actionUserId);


}