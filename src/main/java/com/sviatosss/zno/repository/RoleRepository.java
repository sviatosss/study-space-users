package com.sviatosss.zno.repository;

import com.sviatosss.zno.entity.Enums.ERole;
import com.sviatosss.zno.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}