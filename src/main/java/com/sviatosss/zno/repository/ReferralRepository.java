package com.sviatosss.zno.repository;

import com.sviatosss.zno.entity.Referral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReferralRepository extends JpaRepository<Referral, Long> {
    Optional<Referral> findById(Long id);
    Optional<Referral> findByIp(String ip);
}