package com.sviatosss.zno.service;

import com.sviatosss.zno.entity.Enums.ESubject;
import com.sviatosss.zno.entity.Statistic;
import com.sviatosss.zno.exception.NotFoundException;
import com.sviatosss.zno.repository.StatisticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StatisticService {
    @Autowired
    private StatisticRepository repository;

    @Autowired
    private UserService userService;

    public Statistic findById(Long id){
        return repository.findById(id).orElseThrow(() -> new NotFoundException("Statistic with id - " + id + " not found"));
    }

    public List<Statistic> findUserStatistic() {
        return repository.findAllByUserId(userService.getCurrentUserFromSecurity().getId());
    }
    public List<Statistic> findUserStatisticBySubject(ESubject subject) {
        return repository.findAllByUserIdAndSubject(userService.getCurrentUserFromSecurity().getId(), subject);
    }

    public Statistic create(Statistic statistic){
        return repository.save(statistic);
    }

//    public Statistic update(Long id, StatisticRequest StatisticRequest){
//        Statistic Statistic = findById(id);
//        Statistic.update(StatisticRequest);
//        return repository.save(Statistic);
//    }

    public void deleteById(Long id){
        repository.deleteById(id);
    }
}