package com.sviatosss.zno.service;


import com.sviatosss.zno.entity.Enums.ERole;
import com.sviatosss.zno.entity.Role;
import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.exception.NotFoundException;
import com.sviatosss.zno.payload.request.LoginRequest;
import com.sviatosss.zno.payload.request.SignUpRequest;
import com.sviatosss.zno.payload.response.JwtResponse;
import com.sviatosss.zno.payload.response.MessageResponse;
import com.sviatosss.zno.repository.RoleRepository;
import com.sviatosss.zno.repository.UserRepository;
import com.sviatosss.zno.security.jwt.JwtUtils;
import com.sviatosss.zno.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;


@Service
public class UserAuthService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserService userService;

    public ResponseEntity<?> signIn(LoginRequest loginRequest){

        Optional<User> optionalUser = userRepository.findByPhone(loginRequest.getPhone());
        if (!optionalUser.isPresent()) return new ResponseEntity<>(
                "phone - " + loginRequest.getPhone() + " not found !" , HttpStatus.NOT_FOUND);
        User user = optionalUser.get();

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getPhone(),
                user.getEmail(),
                roles));
    }

    public JwtResponse signUp(SignUpRequest signUpRequest){
        AtomicBoolean isTeacher = new AtomicBoolean(false);

//        if (userRepository.existsByUsername(signUpRequest.getUsername()))
//            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
//
//        if (userRepository.existsByPhone(signUpRequest.getPhone()))
//            return ResponseEntity.badRequest().body(new MessageResponse("Error: Phone is already in use!"));
//
//        if (signUpRequest.getReferral() != null) {
//            if (!userRepository.existsByUsername(signUpRequest.getReferral()))
//                return ResponseEntity.badRequest().body(new MessageResponse("Error: Referral not found !"));
//        }
//
//        if (signUpRequest.getReferral() != null) {
//            if (userRepository.existsByEmail(signUpRequest.getEmail()))
//                return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
//        }


        User user = new User();
        boolean isEmail = false;
        if (signUpRequest.getEmail() != null) {
            isEmail = true;


            // Create new user's account
            user = new User(signUpRequest.getUsername(),
                    signUpRequest.getPhone(),
                    signUpRequest.getEmail(),
                    encoder.encode(signUpRequest.getPassword()));
        }else {
            user = new User(signUpRequest.getUsername(),
                    signUpRequest.getPhone(),
                    encoder.encode(signUpRequest.getPassword()));
        }

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_STUDENT)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "teacher":
//                        if (hasAdminRole){
                        Role adminRole = roleRepository.findByName(ERole.ROLE_STUDENT)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);
                        isTeacher.set(true);
                        break;
//                        }
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_STUDENT)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);

        userRepository.save(user);

//        try {
//            userRepository.save(user);
//        } catch (DataIntegrityViolationException ex) {
//            System.out.println("error");
//            throw new DataIntegrityViolationException(Arrays.toString(ex.getStackTrace()));
//        }


        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(signUpRequest.getUsername(), signUpRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles_list = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        if (isEmail) return new JwtResponse(jwt,
                    user.getId(),
                    user.getUsername(),
                    user.getPhone(),
                    user.getEmail(),
                    roles_list);
        else return new JwtResponse(jwt,
                user.getId(),
                user.getUsername(),
                user.getPhone(),
                roles_list);
    }

    public boolean validUsername(String username){
        return !userRepository.existsByUsername(username);
    }

    public boolean validPhone(String phone){
        return !userRepository.existsByPhone(phone);
    }

    public boolean validEmail(String email){
        return !userRepository.existsByEmail(email);
    }

    public String updateJWT(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtUtils.generateJwtToken(authentication);
    }
}
