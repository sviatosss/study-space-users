package com.sviatosss.zno.service;

import com.sviatosss.zno.entity.ChatRoom;
import com.sviatosss.zno.repository.ChatRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChatRoomService {

    @Autowired
    private ChatRoomRepository chatRoomRepository;

    private List<Long> sort(Long senderId, Long recipientId){
        List<Long> list = new ArrayList<>();
        if (recipientId < senderId) {
            list.add(recipientId);
            list.add(senderId);
        } else {
            list.add(senderId);
            list.add(recipientId);
        }
        return list;
    }

    public String getChatId(Long senderId, Long recipientId) {

        List<Long> positions = sort(senderId, recipientId);

         Optional<ChatRoom> chatRoom = chatRoomRepository.findByChatIdAndSenderIdAndRecipientId(
                 positions.get(0) + "_" + positions.get(1), senderId, recipientId);

         if (chatRoom.isPresent()){
             return chatRoom.get().getChatId();
         }else {

             String chatId = positions.get(0) + "_" + positions.get(1);

             ChatRoom senderRecipient = new ChatRoom(chatId, senderId, recipientId);
//             ChatRoom recipientSender = new ChatRoom(chatId, recipientId, senderId);

             chatRoomRepository.save(senderRecipient);
//             chatRoomRepository.save(recipientSender);

             return chatId;
         }
    }
}
