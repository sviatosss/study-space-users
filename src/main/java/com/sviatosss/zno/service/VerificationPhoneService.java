package com.sviatosss.zno.service;

import com.sviatosss.zno.entity.VerificationPhone;
import com.sviatosss.zno.repository.VerificationPhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VerificationPhoneService {
    @Autowired
    private VerificationPhoneRepository verificationPhoneRepository;

    public void save(String phone, String deviceId){
        verificationPhoneRepository.save(new VerificationPhone(phone, deviceId));
    }

    public Long delete(String phone, String deviceId){
        return verificationPhoneRepository.deleteByPhoneAndDeviceId(phone, deviceId);
    }
}
