package com.sviatosss.zno.service;

import com.sviatosss.zno.entity.Referral;
import com.sviatosss.zno.repository.ReferralRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ReferralService {
    @Autowired
    private UserService userService;

    @Autowired
    private ReferralRepository referralRepository;

    public Referral getById(Long id){
        Optional<Referral> referral = referralRepository.findById(id);
        if (referral.isPresent()){
            return referral.get();
        }
        return null;
    }

    public Optional<Referral> getByIp(String ip){
        return referralRepository.findByIp(ip);
    }

    public void save(Referral referral) {
        Optional<Referral> referralOld = getByIp(referral.getIp());
        if (!referralOld.isPresent())
            referralRepository.save(referral);
        else {
            referralOld.get().setUsername(referral.getUsername());
            referralOld.get().setUserAgent(referral.getUserAgent());
            referralRepository.save(referralOld.get());
        }
    }

    public void checkReferral(String ip){
        Optional<Referral> referral = getByIp(ip);
        if (referral.isPresent()){
            userService.addReferral(referral.get().getUsername());
            remove(referral.get().getId());
        }
    }

    public void remove(Long id) {
        referralRepository.delete(getById(id));
    }
}
