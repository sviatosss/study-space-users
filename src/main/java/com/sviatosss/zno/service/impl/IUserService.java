package com.sviatosss.zno.service.impl;

import com.sviatosss.zno.entity.User;

public interface IUserService {

    void createVerificationToken(User user, String token);
    boolean verificationToken(Long id, String VerificationToken);
}