package com.sviatosss.zno.service;


import com.sviatosss.zno.entity.Enums.ERelationshipStatus;
import com.sviatosss.zno.entity.Relationship;
import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.repository.RelationshipRepository;
import com.sviatosss.zno.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FriendService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private RelationshipRepository relationshipRepository;
    
    public List<User> getFriends(){
        User user = userService.getCurrentUserFromSecurity();
        List<User> friends = new ArrayList<>();
        List<Long> ids = new ArrayList<>();
        List<Relationship> relationships = relationshipRepository.findFriends(
                user.getId(), ERelationshipStatus.ACCEPTED);

        for (Relationship relationship: relationships) {
            if (relationship.getUserOneId() != user.getId()) ids.add(relationship.getUserOneId());
            else ids.add(relationship.getUserTwoId());
        }
        if (relationships.size() != 0){
            friends = userRepository.findAllByIdIn(ids);
        }
        return friends;
    }

//    public boolean addFriend(Long friendId) {
//        List<Long> list = getIdByPosition(friendId);
//        Long userIdOne = list.get(0);
//        Long userIdTwo = list.get(1);
//        Long currentUserId = list.get(2);
//        if (userIdOne != userIdTwo){
//            relationshipRepository.save(new Relationship(userIdOne, userIdTwo, ERelationshipStatus.PENDING, currentUserId));
//            return true;
//        }
//        return false;
//    }

    public Long setFriendshipStatus(Long friendId, ERelationshipStatus status) {
        List<Long> list = getIdByPosition(friendId);
        Long userIdOne = list.get(0);
        Long userIdTwo = list.get(1);
        Long currentUserId = list.get(2);

        boolean legal = false;

//        IMPORTANT ADD SWITCH TO ALL CASE
        switch (status){
            case PENDING: { legal = checkForPending(userIdOne, userIdTwo, friendId); break; }
            case ACCEPTED: { legal = checkForAccepted(userIdOne, userIdTwo, friendId, currentUserId); break; }
            case DECLINED: { legal = checkForDeclined(userIdOne, userIdTwo, friendId); break; }
            case BLOCKED: { legal = checkForBlocked(userIdOne, userIdTwo); break; }
        }
        if (!legal) return null;
        else{
            relationshipRepository.save(new Relationship(userIdOne, userIdTwo, status, currentUserId));
            return currentUserId;
        }
    }

    private boolean checkForPending(Long userIdOne, Long userIdTwo, Long friendId){
        // check if id of friend exist in users table
        boolean friendExist = userRepository.existsById(friendId);
        // if user id not exist - return false
        if (!friendExist) return false;

        // if relationship between users not exist yet - return true
        return ! relationshipRepository.existsByUserOneIdAndUserTwoId(userIdOne, userIdTwo);
    }

    private boolean checkForAccepted(Long userIdOne, Long userIdTwo, Long friendId, Long currentUserId){
//        check if relationship between users exist and status is PENDING and Pending request was sent by friend (not current user)
        if (relationshipRepository.existsByUserOneIdAndUserTwoIdAndStatusAndActionUserId(
                userIdOne, userIdTwo, ERelationshipStatus.PENDING, friendId)) return true;

//        check if relationship between users exist and status is DECLINED and DECLINED request was sent by current User (not friend user)
        else return relationshipRepository.existsByUserOneIdAndUserTwoIdAndStatusAndActionUserId(
                userIdOne, userIdTwo, ERelationshipStatus.DECLINED, currentUserId);
    }

    private boolean checkForDeclined(Long userIdOne, Long userIdTwo, Long friendId){
//        check if relationship between users exist and status is PENDING and Pending request was sent by friend (not current user)
        return  relationshipRepository.existsByUserOneIdAndUserTwoIdAndStatusAndActionUserId(
                userIdOne, userIdTwo, ERelationshipStatus.PENDING, friendId);
    }

    private boolean checkForBlocked(Long userIdOne, Long userIdTwo){
//        check if relationship between users exist and status is ACCEPTED
        return  relationshipRepository.existsByUserOneIdAndUserTwoIdAndStatus(
                userIdOne, userIdTwo, ERelationshipStatus.ACCEPTED);
    }

    private List<Long> getIdByPosition(Long friendId){
        List<Long> list = new ArrayList<>();
        Long currentUserId = userService.getCurrentUserFromSecurity().getId();
        if (currentUserId < friendId) {
            list.add(currentUserId);
            list.add(friendId);
        } else {
            list.add(friendId);
            list.add(currentUserId);
        }
        list.add(currentUserId);
        return list;
    }
}
