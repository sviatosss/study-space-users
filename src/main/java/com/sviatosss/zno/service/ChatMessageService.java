package com.sviatosss.zno.service;

import com.sviatosss.zno.entity.ChatMessage;
import com.sviatosss.zno.entity.Enums.MessageStatus;
import com.sviatosss.zno.exception.NotFoundException;
import com.sviatosss.zno.repository.ChatMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChatMessageService {
    @Autowired
    private ChatMessageRepository repository;
    @Autowired
    private ChatRoomService chatRoomService;


    public ChatMessage save(ChatMessage chatMessage) {
        chatMessage.setStatus(MessageStatus.RECEIVED);
        repository.save(chatMessage);
        return chatMessage;
    }

    public long countNewMessages(Long senderId, Long recipientId) {
        return repository.countBySenderIdAndRecipientIdAndStatus(
                senderId, recipientId, MessageStatus.RECEIVED);
    }

    @Transactional
    public List<ChatMessage> findChatMessages(Long senderId, Long recipientId) {
        Optional<String> chatId = Optional.ofNullable(chatRoomService.getChatId(senderId, recipientId));

        List<ChatMessage> messages = chatId.map(cId -> repository.findByChatId(cId)).orElse(new ArrayList<>());

        if(messages.size() > 0) {
            repository.updateStatus(senderId, recipientId, MessageStatus.DELIVERED);
        }

        return messages;
    }

    public ChatMessage findById(Long id) {
        return repository
                .findById(id)
                .map(chatMessage -> {
                    chatMessage.setStatus(MessageStatus.DELIVERED);
                    return repository.save(chatMessage);
                })
                .orElseThrow(() ->
                        new NotFoundException("can't find message (" + id + ")"));
    }

//    public void updateStatuses(Long senderId, Long recipientId, MessageStatus status) {
//        repository.updateStatus(senderId, recipientId, status.getValue());
//    }
}
