package com.sviatosss.zno.service;


import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.entity.VerificationToken;
import com.sviatosss.zno.exception.NotFoundException;
import com.sviatosss.zno.payload.request.UserUpdateRequest;
import com.sviatosss.zno.repository.UserRepository;
import com.sviatosss.zno.repository.VerificationTokenRepository;
import com.sviatosss.zno.security.services.UserDetailsImpl;
import com.sviatosss.zno.service.additional.email.MailService;
import com.sviatosss.zno.service.impl.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Random;


@Service
@Transactional
public class UserService implements IUserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private MailService mailService;

    public User get(long id) {
        return  userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User - " + id + " not found"));
    }

    public User getUserByUsername(String username){
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException("User - " + username + " not found"));
    }

    public User updateUser(UserUpdateRequest userUpdateRequest){
        User current = getCurrentUserFromDB();
        if (userUpdateRequest.getEmail() != null)
            current.setEmail(userUpdateRequest.getEmail());
        if (userUpdateRequest.getAvatar()  != null)
            current.setAvatar(userUpdateRequest.getAvatar());
        return userRepository.save(current);
    }

    public User getCurrentUserFromDB(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return getUserByUsername(userDetails.getUsername());
    }

    public User getCurrentUserFromSecurity(){
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new User(userDetails);
    }

    public void addReferral(String username){
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException("User  -  " + username + " not found"));
        user.setReferral(user.getReferral()+1);
        userRepository.save(user);
    }

    public User findByUsernameOrPhone(String search){
        Optional<User> user = userRepository.findAllByUsernameOrPhone(search, search);
        if (user.isPresent()){
            return user.get();
        }return null;
    }

    @Override
    public boolean verificationToken(Long id, String VerificationToken) {
        Long verificationTokenId = tokenRepository.deleteByTokenAndAndUser(VerificationToken, new User(id));
        if (verificationTokenId != 0){
            userRepository.updateActive(id);
            System.out.println(verificationTokenId);
            return true;
        }else return false;
    }

    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    public int updatePasswordByEmail(String email){
        Random random = new Random();
        String password = String.valueOf(100000 + random.nextInt(900000));
        System.out.println(password);
        mailService.sendingPasswordByEmail(email, password);
        return userRepository.updatePasswordByEmail(encoder.encode(password), email);
    }

    public int updatePasswordByPhone(String phone){
        Random random = new Random();
        String password = String.valueOf(100000 + random.nextInt(900000));
        return userRepository.updatePasswordByPhone(encoder.encode(password), phone);
    }


}