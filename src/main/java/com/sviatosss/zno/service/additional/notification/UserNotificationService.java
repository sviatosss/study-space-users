package com.sviatosss.zno.service.additional.notification;
import com.sviatosss.zno.entity.Button;
import com.sviatosss.zno.entity.User;
import com.sviatosss.zno.entity.UserNotification;
import com.sviatosss.zno.repository.UserRepository;
import com.sviatosss.zno.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserNotificationService {

	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	public void sendFriendRequestNotification(UserNotification userNotification){
		System.out.println("friend request sending !");

		userNotification.setConfirm(new Button("додати друга", "/friend/accept/" + userNotification.getSenderId()));
		userNotification.setReject(new Button("відхилити запит", "/friend/deny/" + userNotification.getSenderId()));

		send(userNotification);
	}

	private void send(UserNotification userNotification){
		messagingTemplate.convertAndSendToUser(
				String.valueOf(userNotification.getRecipientId()),"/queue/messages",
				userNotification);
	}
}
