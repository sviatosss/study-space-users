package com.sviatosss.zno.service.additional.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {

    private TemplateEngine templateEngine;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String buildVerification(String url) {
        Context context = new Context();
        context.setVariable("url", url);
        return templateEngine.process("welcome", context);
    }

    public String buildPassword(String password) {
        Context context = new Context();
        context.setVariable("password", password);
        return templateEngine.process("password", context);
    }
}
