package com.sviatosss.zno.service.additional.email;


import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;


@Service
public class MailService {

    @Autowired
    ObjectFactory<HttpSession> httpSessionFactory;

    @Autowired
    private MailContentBuilder mailContentBuilder;

    private JavaMailSender mailSender;

    @Autowired
    public MailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void sendingEmailVerification(String email, Long userId, String token){
        String url = "localhost:8080/user/" + userId + "/email-verification/" + token;
        prepareAndSendVerification(email, url);
    }

    public void sendingPasswordByEmail(String email, String password){
        prepareAndSendPassword(email, password);
    }

    private void prepareAndSendVerification(String recipient, String url) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("appointment.appss@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Please, approve your email !");
            String content = mailContentBuilder.buildVerification(url);
            messageHelper.setText(content, true);
        };
        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            // runtime exception; compiler will not force you to handle it
        }
    }

    private void prepareAndSendPassword(String recipient, String password) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("appointment.appss@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("New password !");
            String content = mailContentBuilder.buildPassword(password);
            messageHelper.setText(content, true);
        };
        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            // runtime exception; compiler will not force you to handle it
        }
    }
}
