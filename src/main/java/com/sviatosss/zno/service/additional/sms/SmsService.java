package com.sviatosss.zno.service.additional.sms;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SmsService {
	public void send(String phone, String code){
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://api.mobizon.ua/service/message/sendSmsMessage?output=json&api=v1&apiKey=" +
				"ua71d6bbde1d7e986589bf721867a20e69212e5dc949c5c41e8c04cdec6c07ed3c7bad");

		httpPost.addHeader("content-type", "application/x-www-form-urlencoded");
		httpPost.addHeader("cache-control", "no-cache");

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("recipient", phone));
		params.add(new BasicNameValuePair("text", "Code: " + code));
//		params.add(new BasicNameValuePair("from", "Z helper"));

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		CloseableHttpResponse response = null;
		try {
			response = client.execute(httpPost);
		} catch (IOException e) {
			e.printStackTrace();
		}

		assert response != null;
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
