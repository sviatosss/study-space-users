package com.sviatosss.zno.entity;

import com.sviatosss.zno.entity.Enums.ESubject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "statistic")
public class Statistic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;
    private String testWrapID;
    private String testTitle;

    @Enumerated(EnumType.STRING)
    private ESubject subject;

    private Integer correct;
    private Integer wrong;
    private Integer missed;

    private Integer total;
}
