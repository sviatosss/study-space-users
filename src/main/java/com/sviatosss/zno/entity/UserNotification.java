package com.sviatosss.zno.entity;
import com.sviatosss.zno.entity.Enums.ЕTypeNotification;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserNotification {
    private Long id;
    private Long senderId;
    private String senderUsername;
    private Long recipientId;
    private ЕTypeNotification type;

    private Button confirm;
    private Button reject;

    public UserNotification(Long id, Long senderId, String senderUsername) {
        this.id = id;
        this.senderId = senderId;
        this.senderUsername = senderUsername;
    }

    public UserNotification(Long id, Long senderId, String senderUsername, Long recipientId, ЕTypeNotification type) {
        this.id = id;
        this.senderId = senderId;
        this.senderUsername = senderUsername;
        this.recipientId = recipientId;
        this.type = type;
    }

    public UserNotification(Long id, String senderUsername, Long recipientId, ЕTypeNotification type) {
        this.id = id;
        this.senderUsername = senderUsername;
        this.recipientId = recipientId;
        this.type = type;
    }
}
