package com.sviatosss.zno.entity;

import com.sviatosss.zno.entity.Enums.MessageStatus;
import lombok.*;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "chat_messages")
public class ChatMessage {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   private String chatId;
   private Long senderId;
   private Long recipientId;
   private String senderUsername;
   private String recipientUsername;
   private String content;
   private Date timestamp;
   private MessageStatus status;
}