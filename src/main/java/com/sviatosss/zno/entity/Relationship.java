package com.sviatosss.zno.entity;

import com.sviatosss.zno.entity.Enums.ERelationshipStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name = "relationships")
@IdClass(RelationshipId.class)
public class Relationship {
    @Id
    private Long userOneId;

    @Id
    private Long userTwoId;

    @Enumerated(EnumType.ORDINAL)
    private ERelationshipStatus status;

    private Long actionUserId;

    Relationship(){}
}