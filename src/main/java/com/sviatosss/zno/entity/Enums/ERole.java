package com.sviatosss.zno.entity.Enums;

public enum ERole {
    ROLE_STUDENT,
    ROLE_TEACHER,
    ROLE_CONTENT_MANAGER,
    ROLE_ADMIN
}