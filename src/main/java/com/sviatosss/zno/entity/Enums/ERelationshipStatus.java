package com.sviatosss.zno.entity.Enums;

public enum ERelationshipStatus {
    PENDING,
    ACCEPTED,
    DECLINED,
    BLOCKED
}
