package com.sviatosss.zno.entity.Enums;

public enum ESubject {
    GEOGRAPHY,
    HISTORY_OF_UKRAINE,
    UKRAINIAN_LANGUAGE,
    UKRAINIAN_LITERATURE,
    MATHEMATICS,
    ENGLISH,
    BIOLOGY,
    PHYSICS,
    CHEMISTRY
}