package com.sviatosss.zno.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sviatosss.zno.security.services.UserDetailsImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
@Getter
@Setter
@AllArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "username is required")
    @Column(unique = true)
    private String username;

    @NotBlank(message = "phone is required")
    @Column(unique = true)
    private String phone;

//    @NotBlank(message = "email is required")
    @Email
    @Column(unique = true)
    private String email;

    @NotBlank(message = "password is required")
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @Lob
    @Column(name = "profile_image")
    private byte[] avatar;

    @Column(columnDefinition = "boolean default false")
    private boolean online;

    @Column(columnDefinition = "boolean default false")
    private boolean active;

    @Column(columnDefinition = "boolean default false")
    private boolean premium;

    @Column(columnDefinition = "integer default 0")
    private int referral;

    public User() {}

    public User(Long id, String username, String phone) {
        this.id = id;
        this.username = username;
        this.phone = phone;
    }

    public User(String username, String phone, String password) {
        this.username = username;
        this.phone = phone;
        this.password = password;
    }

    public User(String username, String phone, String email, String password) {
        this.username = username;
        this.phone = phone;
        this.password = password;
        this.email = email;
    }

    public User(Long id, String username, String phone, String email) {
        this.id = id;
        this.username = username;
        this.phone = phone;
        this.email = email;
    }

    public User(Long id) {
        this.id = id;
    }

    public User(UserDetailsImpl userDetails) {
        this.id = userDetails.getId();
        this.phone = userDetails.getPhone();
        this.username = userDetails.getUsername();
        this.password = userDetails.getPassword();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                ", avatar='" + avatar + '\'' +
                ", online=" + online +
                ", active=" + active +
                ", premium=" + premium +
                ", referral=" + referral +
                '}';
    }
}