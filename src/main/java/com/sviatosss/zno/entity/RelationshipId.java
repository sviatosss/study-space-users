package com.sviatosss.zno.entity;

import java.io.Serializable;

public class RelationshipId implements Serializable {
    private Long userOneId;

    private Long userTwoId;

    public RelationshipId(Long userOneId, Long userTwoId) {
        this.userOneId = userOneId;
        this.userTwoId = userTwoId;
    }
    public RelationshipId(){}
}
