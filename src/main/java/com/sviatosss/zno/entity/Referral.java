package com.sviatosss.zno.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "referral")
public class Referral {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String ip;
    private String userAgent;
    private String username;

    public Referral(String ip, String userAgent, String username) {
        this.ip = ip;
        this.userAgent = userAgent;
        this.username = username;
    }

    @Override
    public String toString() {
        return "Referral{" +
                "ip='" + ip + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
