package com.sviatosss.zno.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JwtResponse {
    private String token;
    private Long id;
    private String username;
    private String email;
    private String phone;
    private List<String> roles;

    public JwtResponse(String accessToken, Long id, String username, String phone, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.phone = phone;
        this.roles = roles;
    }
    public JwtResponse(String accessToken, Long id, String username, String phone, String email, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.phone = phone;
        this.email = email;
        this.roles = roles;
    }
}
