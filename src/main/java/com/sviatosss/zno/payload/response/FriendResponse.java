package com.sviatosss.zno.payload.response;

import com.sviatosss.zno.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
public class FriendResponse {
    private Long id;
    private String username;
    private String phone;
    private byte[] avatar;
    private boolean online;
    private boolean premium;
    private int referral;

    public FriendResponse() {}

    private FriendResponse(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.phone = user.getPhone();
        this.avatar = user.getAvatar();
        this.online = user.isOnline();
        this.premium = user.isPremium();
        this.referral = user.getReferral();
    }

    public static List<FriendResponse> createFromList(List<User> userList) {
        List<FriendResponse> friendResponseList = new ArrayList<>();
        for (User user: userList) {
            friendResponseList.add(new FriendResponse(user));
        }
        return friendResponseList;
    }
}