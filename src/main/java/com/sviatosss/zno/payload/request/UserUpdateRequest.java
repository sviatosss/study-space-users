package com.sviatosss.zno.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserUpdateRequest {
    private String email;
    private byte[] avatar;
}
