package com.sviatosss.zno.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class SignUpRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Size(min = 9, max = 9)
    private String phone;

    @Email
    private String email;

    private String referral;

    private Set<String> role;

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    public SignUpRequest(SignUpWebRequest signUpWebRequest) {
        this.username = signUpWebRequest.getUsername();
        this.phone = signUpWebRequest.getPhone();
        this.email = signUpWebRequest.getEmail();
        this.referral = signUpWebRequest.getReferral();
        this.role = signUpWebRequest.getRole();
        this.password = signUpWebRequest.getPassword();
    }
}
