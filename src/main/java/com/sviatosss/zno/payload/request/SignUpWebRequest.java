package com.sviatosss.zno.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class SignUpWebRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    @NotBlank
    @Size(min = 9, max = 9)
    private String phone;

    private int otpnum;

    @Email
    private String email;

    private String referral;

    private Set<String> role;

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
}
